#!/bin/env perl
=head1 NAME

	sam2list.pl - read as STDIN a SAM alignment, and count per block and per reference the number of tags BX:Z:

=head1 SYNOPSIS

	sam2list.pl [--help] [--man] [--block-size int] [--min-tags int]

=head1 DESCRIPTION

	This program reads a SAM alignment as STDIN
	It will count all tags BX:Z: per ref and per block of size --block-size
	Tags with less than --min-tags will be discarded
	Reports as STDOUT tag count per block and reference

=head2 INPUT

	STDIN SAM alignment

=head2 OUTPUT

	STDOUT tag count per block and reference:
		ref_name|block_number <tab> tagA:countA[,tagB:countB]*
		with 0 <= block_number < ceil(ref_size/block-size)

=head1 OPTIONS

=over

=item B<--help>

	This text.

=item B<--man>

	Extended help

=item B<--block-size> int 

	Size of the block, default 10000 bases

=item B<--min-tags> int

	Tag's count under which tag is dicarded, default 6

=back

=head1 AUTHORS

	VERSION: 1.2
	AUTHORS: Patrice Dehais - SIGENAE Toulouse (sigenae-support@listes.inra.fr)
	COPYRIGHT: 2019 INRA
	LICENSE: GNU GPLv3

=cut

use strict;
use Getopt::Long;
use Pod::Usage;
use POSIX;

#==============================================================================

# Options definitions (name,format,default,required)
my @Options = (
		['help',undef,undef,0],
		['man',undef,undef,0],
		['block-size','i',10000,0],
		['min-tags','i',6,0],
		);
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(verbose=>2) if ($Values{'man'});
# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;
map {pod2usage("$_ must be > 0") if ($Values{$_} <= 0)} qw( block-size min-tags );

my %Ref=();
my $block_size=$Values{'block-size'};
my $min_tags=$Values{'min-tags'};
while(my $line=<STDIN>){
	chomp $line;
	$line=~/BX:Z:(\S+)/||next;
	my $tag=uc($1);
	my @F=split/\t/,$line;
	next if($F[1]&4); # read unmapped
	my $block_number=ceil($F[3]/$block_size)-1;
	$Ref{$F[2]}[$block_number]{$tag}++;
}
foreach my $r (keys %Ref){
	my $nb=scalar(@{$Ref{$r}});
	for (my $i=0;$i<$nb;$i++){
		printf STDOUT "%s|%d\t%s\n",$r,$i,join(',',map{$Ref{$r}[$i]{$_}>=$min_tags?"$_:$Ref{$r}[$i]{$_}":()} keys %{$Ref{$r}[$i]});
	}
}
