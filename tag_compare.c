#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <pthread.h>

#define MAX_LINE 5000000
#define INIT_NB_BLOCKS 100000

/*
 * Purpose: read a text file given as stdin with many lines which define each a block: 
 * line ::= block_id <tab> tags*
 * block_id ::= <string>
 * tags ::= tag[,tag]*
 * tag ::= seq"-"id":"nb
 * seq ::= [base]+
 * base ::= "A" | "T" | "G" | "C"
 * id ::= <int>
 * nb ::= <int>
 * 
 * It will store all blocks in a compact way and report comparison of blocks two by two in parallel (param -t nb_threads)
 * in nb_threads gzipped outfiles "tag_compare-out#####.gz" foreach couple of blocks gives the number of tags they share
 * block_id1 <tab> block_id2 <tab> nb
 * diagonal is included: block is compared to itself
 */

/*
 * a 32bp sequence tag + its indice are store in a unsigned 64bits int + a char for its length + an int for its indice
 */
typedef struct Tag {
	char lg;
	unsigned long long key;
	int ind;
} Tag;

/*
 * a way to store all tags associated to a block
 */
typedef struct TagList TagList;

struct TagList {
	Tag tag;
	TagList *next_tag;
};

/*
 * a block is defined by a name and a list of tags
 */
typedef struct Block {
	char *name;
	TagList *tag_list;
	int nbtag;
} Block;

/*
 * convert a nucleic base into two bits int
 */
char base_code(char c){
	switch (c) {
		case 'A':
		case 'a': return(0);
		case 'T':
		case 't': return(1);
		case 'G':
		case 'g': return(2);
		case 'C':
		case 'c': return(3);
		default: fprintf(stdout,"Unknown base %c\n",c); exit(1);
	}
}

/* 
 * takes a string tag sequence [ATGC]{0,32} as input, and return corresponding numeric long int 
 */
void tag_encode(Tag *tag,char *s, char *i){
	tag->lg=0;
	tag->key=0;
	tag->ind=atoi(i);
	char *c;
	
	if(strlen(s)>32){
		fprintf(stderr,"Tag too long (max 32bp): %s\n",s);
		exit(1);
	}
	for(c=s;*c;c++){
		tag->key = tag->key<<2;
		tag->key+=base_code(c[0]);
		tag->lg++;
	}
}

/*
 * allocate blocks storage space
 */
Block *alloc_blocks(Block *blocks, int nb) {
	Block *b;
	b=realloc(blocks,(size_t)(nb*sizeof(Block)));
	if(b==NULL){
		fprintf(stderr, "Can't allocate %d blocks\n", nb);
		exit(1);
	}
	return(b);
}

/*
 * return a pointer to the character in a string next to the one specified, and replace it by 0
 */
char *reach_char(char *s, char c){
	char *p=s;
	while(*p){
		if(*p==c){
			*p=0;
			return(p+1);
		}
		p++;
	}
	return(NULL);
}

/*
 * sort comparison function for tags
 */
int comptag(Tag a, Tag b){
	if (a.lg > b.lg) {
		return(1);
	}
	if (a.lg < b.lg) {
		return(-1);
	}
	if (a.key > b.key) {
		return(1);
	}
	if (a.key < b.key) {
		return(-1);
	}
	if (a.ind > b.ind) {
		return(1);
	}
	if (a.ind < b.ind) {
		return(-1);
	}
	return(0);
}

/*
 * add a tag into a TagList of a block
 */
void add_tag(TagList **tl, char *tagstr, char *ind){
	TagList *cell,*p1,*p2;
	if((cell=malloc(sizeof(TagList)))==NULL){
		fprintf(stderr,"Memory allocation failed while adding tag\n");
		exit(1);
	}
	tag_encode(&cell->tag,tagstr,ind);
	cell->next_tag=NULL;
	if (*tl == NULL){ /* new list */
		*tl=cell;
		return;
	}
	/* insert new cell in increasing tag order */
	p1=*tl;
	p2=NULL;
	while (p1!=NULL && comptag(cell->tag,p1->tag)==1){ // incr order
		p2=p1;
		p1=p1->next_tag;
	}
	if (p2==NULL){ /* cell will be the first */
		*tl=cell;
	} else { /* cell follows p2 */
		p2->next_tag=cell;
	}
	if (p1!=NULL){ /* cell before p1 */
		cell->next_tag=p1;
	}
}

/*
 * split a line definition block and store it
 */
void store_block(Block *blocks, int no_block, char *line){
	char *tag;
	
	if ((tag=reach_char(line,'\t'))==NULL){
		fprintf(stderr,"Block definition line should contain a tab to delimit block_id and tags:\t%s\n",line);
		exit(1);
	}
	if((blocks[no_block].name = strdup(line)) == NULL){
		fprintf(stderr,"Can't store name of block %d : %s\n",no_block+1,line);
		exit(1);
	}
	blocks[no_block].tag_list=NULL;
	blocks[no_block].nbtag=0;
	while(tag!=NULL && *tag){
		char *seq,*ind;
		seq=tag;
		if((ind=reach_char(tag,'-'))==NULL){
			fprintf(stderr,"Bad tag format for block %d %s: %s\n",no_block-1,line,tag);
			exit(1);
		}
		tag=reach_char(ind,',');
		add_tag(&blocks[no_block].tag_list,seq,ind);
		blocks[no_block].nbtag++;
	}
}

/* just print on stderr progress info */
void info(char *msg){
	char buff[1024];
	time_t now;
	time(&now);
	strcpy(buff,ctime(&now));
	buff[strlen(buff)-1]=0;
	fprintf(stderr,"%s: %s\n",buff,msg);
}

/* global data available for threads */
static double *pos;
static FILE **outfiles;
static Block *blocks=NULL;
static int nb_blocks;
static int min_shared=1;

/*
 * report the number of common tags between two bloks
 */
void compblocks(FILE *fout,Block b1,Block b2){
	TagList *t1,*t2;
	t1=b1.tag_list;
	t2=b2.tag_list;
	int nb=0;
	
	while (t1 != NULL && t2 != NULL){
		int cmp=comptag(t1->tag,t2->tag);
		switch (cmp) {
			case 0: /* t1 equal t2 */
				nb++;
				t1=t1->next_tag;
				t2=t2->next_tag;
				break;
			case 1: /* t1 greater than t2 */
				t2=t2->next_tag;
				break;
			case -1: /* t1 smaller than t2 */
				t1=t1->next_tag;
				break;
			default: fprintf(stderr,"strange");
		}
	}
	if(nb>=min_shared){
		fprintf(fout,"%s\t%s\t%d\n",b1.name,b2.name,nb);
	}
}

/* thread function */
static void *block_batch(void *p_data)
{
   int n = (int)(p_data);
   int i,j;
   
	for(i=pos[n];i<=pos[n+1]-1;i++){
		for(j=i;j<nb_blocks;j++){ /* include diagonal */
			compblocks(outfiles[n],blocks[i],blocks[j]);
		}
	}
}


int main(int argc,char *argv[])
{
	char buffer[MAX_LINE];
	int max_blocks=INIT_NB_BLOCKS;
	int i,j;
	int nb_threads=1;
	pthread_t *my_threads;
	
	/* read parameters */
	char *str_threads=NULL;
	if(argc != 5){
		fprintf(stderr,"Usage: %s -m min-shared -t nb_threads\n",argv[0]);
		exit(1);
	}
	for(i=1;i<argc;i++){
		if(strcmp(argv[i],"-m")==0){
			if((min_shared=atoi(argv[++i]))<=0){
				fprintf(stderr,"min-shared (-m)  must be >0\n");
				exit(1);
			}
		}
		else if(strcmp(argv[i],"-t")==0){
			if((nb_threads=atoi(argv[++i]))<=0){
				fprintf(stderr,"threads (-t) must be >0\n");
				exit(1);
			}
		} else {
			fprintf(stderr,"Unknown option %s\n",argv[i]);
			exit(1);
		}
	}
	
	/* allocation for threads data */
	info("Alloc multithreading data structures");
	if ((pos=calloc(nb_threads+1,sizeof(double)))==NULL){
		fprintf(stderr,"Can't allocate interval data structure for multithreading\n");
		exit(1);
	}
	if ((outfiles=calloc(nb_threads,sizeof(FILE*)))==NULL){
		fprintf(stderr,"Can't allocate output files data structure for multithreading\n");
		exit(1);
	}
	if ((my_threads=calloc(nb_threads,sizeof(pthread_t)))==NULL){
		fprintf(stderr,"Can't allocate threads data structure for multithreading\n");
		exit(1);
	}
	
	/* firt block storage allocation */
	info("Alloc initial blocks data structure");
	blocks = alloc_blocks(blocks,max_blocks);
	
	/* loop on block lines and store blocks */
	info("Read tag lists");
	while(fgets(buffer, MAX_LINE, stdin)) {
		/* check line is complete */
		int l=strlen(buffer);
		if (buffer[l-1] != '\n'){
			fprintf(stderr,"Incomplete line:\n%s\n",buffer);
			exit(1);
		}
		/* remove trailing new line */
		buffer[l-1]=0;
		nb_blocks++;
		/* check enough space */
		if (nb_blocks>max_blocks) {
			max_blocks+=INIT_NB_BLOCKS;
			blocks = alloc_blocks(blocks,max_blocks);
		}
		/* store block */
		store_block(blocks,nb_blocks-1,buffer);
	}
	info("Tag lists read");
	
	/* compute intervals for mutithreadings */
	info("Split processing for multithreading");
	double A=pow(nb_blocks/sqrt(nb_threads),2);
	pos[0]=0;
	for(i=1;i<=nb_threads;i++){
		pos[i]=sqrt(A+pow(pos[i-1],2));
		sprintf(buffer,"gzip -c > tag_compare-out-%05d.gz",i);
		if((outfiles[i-1]=popen(buffer,"w"))==NULL){
			fprintf(stderr,"Can't open output file for thread %d: %s\n",i,buffer);
			exit(1);
		}
	}
	for(i=0;i<=nb_threads;i++){
		pos[i]=trunc(nb_blocks-pos[i]);
	}
	for(i=0;i<=(nb_threads/2);i++){
		double tmp;
		tmp=pos[nb_threads-i];
		pos[nb_threads-i]=pos[i];
		pos[i]=tmp;
	}
	/* just to be sure about limits because of trunc */
	pos[0]=0;
	pos[nb_threads]=nb_blocks;
	
	/* create threads */
	info("Create threads");
	for(i=0;i<nb_threads;i++){
		if(pthread_create(&my_threads[i],NULL,block_batch,(void*)i)!=0){
			fprintf(stderr,"Failed create thread %d\n",i);
			exit(1);
		}
	}
	
	/* wait for threads termination */
	info("wait for threads");
	for(i=0;i<nb_threads;i++){
		pthread_join(my_threads[i], NULL);
		pclose(outfiles[i]);
	}
	
	info("All threads done");
	exit(0);
}
