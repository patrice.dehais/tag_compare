# Tag_compare

report from a BAM alignment the number of BX:Z: tags shared between to blocks of given size

## Name
Tag_compare

## Description
            tag_compare.pl program reads a BAM file given as parameter --bam-file
            It will count all tags BX:Z: per ref and per block of size --block-size
            Tags with less than --min-tags will be discarded
            Then it compairs blocks two by two and count shared tags (diagonal is included, a block is compared to itself)
            Couple of blocks having less than --min-shared are not reported
        
            Processing has two steps:
            -1- count tags per block in parallel for each reference in the BAM using a Slurm cluster
                creates a Slurm job_array to compute counts via sam2list.pl 
            -2- compare blocks two by two. Multithreading is possible with the --threads parameter
                uses tag_compare binary, then via filter and sort results on the cluster via a srun
        
            A folder for temporary files called YYYYMMDDhhmm-$(basename --bam-file .bam) is created and suppressed on success

 ## INPUT
            the BAM file
 ## OUTPUT
            A compressed 3 tab delimited columns text file (blockA blockB nb_tags_shared) name $(basename bam-file .bam)-tagcmp.tsv
            Block name is formated as follows: ref_name|block_number
            with 0 <= block_number < ceil(ref_size/block-size)
 ## OPTIONS
    --help
                This text.

    --man
                Extended help

    --bam-file string
                Path to the BAM file

    --block-size int
                Size of the block, default 10000 bases

    --min-tags int
                Tag's count under which tag is discarded, default 6

    --min-shared int
                Minimum number of shared tags between to blocks to be reported, default 1

    --threads int
                Number of threads requested, default 1

    --mem-per-cpu string
                Memory required per CPU: amount number followed by unit (MGT), default 6G

## Requirements

Needs samtools, a Slurm cluster and my sarray custom tool

## Installation

tag_compare.c needs to be compiled with pthread lib in its current folder

## Authors and acknowledgment
Patrice Dehais - SIGENAE Toulouse (sigenae-support@listes.inra.fr)

## License
COPYRIGHT: 2019 INRAE - GNU GPLv3

