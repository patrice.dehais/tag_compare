#!/usr/bin/perl -w

# POD documentation - main docs before the code.

=head1 NAME

	tag_compare.pl - report from a BAM alignment the number of BX:Z: tags shared between to blocks of given size

=head1 SYNOPSIS

	tag_compare.pl [--help] [--man] [--bam-file string] [--block-size int] [--min-tags int] [--min-shared int] [--threads int] [--mem-per-cpu xx]

=head1 DESCRIPTION

	This program reads a BAM file given as parameter --bam-file
	It will count all tags BX:Z: per ref and per block of size --block-size
	Tags with less than --min-tags will be discarded
	Then it compairs blocks two by two and count shared tags (diagonal is included, a block is compared to itself)
	Couple of blocks having less than --min-shared are not reported
	
	Processing has two steps:
	-1- count tags per block in parallel for each reference in the BAM using a Slurm cluster
	-2- compare blocks two by two. Multithreading is possible with the --threads parameter
	
	A folder for temporary files called YYYYMMDDhhmm-$(basename --bam-file .bam) is created and suppressed on success

=head2 INPUT

	the BAM file

=head2 OUTPUT

	A compressed 3 tab delimited columns text file (blockA blockB nb_tags_shared) name $(basename bam-file .bam)-tagcmp.tsv
	Block name is formated as follows: ref_name|block_number
	with 0 <= block_number < ceil(ref_size/block-size)


=head1 OPTIONS

=over

=item B<--help>

	This text.

=item B<--man>

	Extended help

=item B<--bam-file> string

	Path to the BAM file

=item B<--block-size> int 

	Size of the block, default 10000 bases

=item B<--min-tags> int

	Tag's count under which tag is discarded, default 6

=item B<--min-shared> int

	Minimum number of shared tags between to blocks to be reported, default 1

=item B<--threads> int

	Number of threads requested, default 1

=item B<--mem-per-cpu> string

	Memory required per CPU: amount number followed by unit (MGT), default 6G

=back

=head1 AUTHORS

	VERSION: 1.3
	AUTHORS: Patrice Dehais - SIGENAE Toulouse (sigenae-support@listes.inra.fr)
	COPYRIGHT: 2019 INRA
	LICENSE: GNU GPLv3

=cut

use strict;
use Getopt::Long;
use Pod::Usage;
use FindBin qw($RealBin);
use File::Basename;
require Time::localtime;
use POSIX qw(strftime);
use File::Path;

#==============================================================================
sub info($){
	my ($msg)=@_;
	printf STDERR "%s: %s\n",Time::localtime::ctime(),$msg;
}

#==============================================================================

# Options definitions (name,format,default,required)
my @Options = (
		['help',undef,undef,0],
		['man',undef,undef,0],
		['bam-file','s',undef,1],
		['block-size','i',10000,0],
		['min-tags','i',6,0],
		['min-shared','i',1,0],
		['threads','i',1,0],
		['mem-per-cpu','s','6G',0],
		);
# defaults values
my %Values=();
my %Required=();
map {$Values{$_->[0]}=$_->[2];$Required{$_->[0]}=$_->[3];} @Options;
# build options list
my %Options=();
map {$Options{defined($_->[1])?$_->[0].'='.$_->[1]:$_->[0]}=\$Values{$_->[0]};} @Options;
# retrieve options
GetOptions(%Options) || pod2usage(2);
pod2usage(1) if ($Values{'help'});
pod2usage(verbose=>2) if ($Values{'man'});
# check required
map {pod2usage("$_ is required") if ($Required{$_}&&!defined($Values{$_}));} keys %Values;
map {pod2usage("$_ must be > 0") if ($Values{$_} <= 0)} qw( block-size min-tags min-shared threads);

# Some controls
system('which samtools &> /dev/null')==0 || die "No samtools command found";
system('which sarray &> /dev/null')==0 ||  die "No sarray command found";
system('which srun &> /dev/null')==0 ||  die "No srun command found";
-x "$RealBin/sam2list.pl" || die "No sam2list.pl command found";
-x "$RealBin/tag_compare" || die "No tag_compare command found";

-r $Values{'bam-file'} || die "File $Values{'bam-file'} not found";
-r "$Values{'bam-file'}.bai" || die "File $Values{'bam-file'}.bai not found";

$Values{'mem-per-cpu'}=~/^\d+[MGT]?$/ || die "--mem-per-cpu option bad format(<int>[MGT]): $Values{'mem-per-cpu'}";

my $bam_file=basename($Values{'bam-file'},'.bam');
my $temp_folder = strftime("%Y%m%d%H%M%S", localtime)."-$bam_file";

mkdir $temp_folder || die "Can't create folder $temp_folder";
my $relative_path_for_link='';
if (substr($Values{'bam-file'},0,1) !~ /[\/~]/){
	$relative_path_for_link='../';
}
symlink "$relative_path_for_link$Values{'bam-file'}","$temp_folder/$bam_file.bam" || die "Can't create symlink for $Values{'bam-file'}";
symlink "$relative_path_for_link$Values{'bam-file'}.bai","$temp_folder/$bam_file.bam.bai" || die "Can't create symlink for $Values{'bam-file'}.bai";

chdir $temp_folder;

# build sarray commands file to generate tag-list files
info("Build sarray command list to count tags");
open(BAM,"samtools view -H $bam_file.bam|") || die "Can't retrieve BAM header from $bam_file.bam";
my %Ref=();
while(my $line=<BAM>){
	$line=~/^@.+SN:(\S+)\s+LN:(\d+)/||next;
	$Ref{$1}=$2;
}
close(BAM);
my @Ref=sort{$Ref{$b}<=>$Ref{$a}}keys %Ref;
my $max_size=$Ref{$Ref[0]};
my $current_size=0;
my @RefGrp=();
my $refgrp=0;

open(SARRAY,">$bam_file.jobs") || die "Can't create file $bam_file.jobs";
foreach my $ref (@Ref){
	if($current_size+$Ref{$ref} <= $max_size){
		push @RefGrp,$ref;
		$current_size+=$Ref{$ref};
	} else {
		printf SARRAY "samtools view -h $bam_file.bam %s | $RealBin/sam2list.pl --min-tags $Values{'min-tags'} --block-size $Values{'block-size'} | gzip -c > refgrp%05d.tag-list.gz\n",
			join(' ',@RefGrp),$refgrp;
		@RefGrp=($ref);
		$refgrp++;
		$current_size=$Ref{$ref};
	}
}
printf SARRAY "samtools view -h $bam_file.bam %s | $RealBin/sam2list.pl --min-tags $Values{'min-tags'} --block-size $Values{'block-size'} | gzip -c > refgrp%05d.tag-list.gz\n",
	join(' ',@RefGrp),$refgrp if(@RefGrp);
close(SARRAY);
info("Command list built");

# generates tag-list on the cluster
info("Count tags");
system("sarray --mem-per-cpu=$Values{'mem-per-cpu'} --wait $bam_file.jobs")==0 || die "sarray submission of $bam_file.jobs failed";
`find . -maxdepth 1 -type f -name \\*.out -not -size 0|wc -l`==0 || die "some Slurm output files are not empty!";
info("Tags counted");

# launch tag comparison on cluster
info("Compare blocks");
my $cmd=sprintf("gunzip -c *.tag-list.gz | $RealBin/tag_compare -m %d -t %d", $Values{"min-shared"}, $Values{"threads"});
system("srun -c $Values{'threads'} bash -c '$cmd'")==0 || die "tag_compare failed";
info("Bocks compared");

# gather results, reformat and sort them
chdir('..');
info("Gather temporary result files");
	open(FOUT,">$temp_folder/tag_compare-sort.sh") || die "can't create $temp_folder/tag_compare-sort.sh";
	#system("gunzip -c $temp_folder/tag_compare-out*.gz | tr '|\t' ' ' | awk '{ if(\$1>\$3){print \$3,\$4,\$1,\$2,\$5} else print }' | sort --parallel=8 -S32G -k1,1d -k3,3d | gzip -c > $bam_file-tagcmp.tsv.gz")==0 || die "failed to gather results into file $bam_file-tagcmp.tsv.gz";
	printf FOUT "gunzip -c $temp_folder/tag_compare-out*.gz | tr '|\t' ' ' | awk '{ if(\$1>\$3){print \$3,\$4,\$1,\$2,\$5} else print }' | sort --parallel=8 -S32G -k1,1d -k3,3d | gzip -c > $bam_file-tagcmp.tsv.gz\n";
	close(FOUT);
system("srun -c 8 --mem=48G bash '$temp_folder/tag_compare-sort.sh'")==0 || die "tag_compare-sort.sh failed";
rmtree $temp_folder;
info("Result file is $bam_file-tagcmp.tsv.gz");
